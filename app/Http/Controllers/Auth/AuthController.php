<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;



class AuthController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function authDev()
    {
        $credentials = ['email' => 'maytee.ru@cmu.ac.th', 'password' => 'maytee.ru@cmu.ac.th'];
        if (Auth::attempt($credentials)) {

            return redirect('index');
        }
    }
}

?>
